package com.gmail.chickenpowerrr.vanish.manager;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The class which manages who are in vanish and handles the vanish logic itself for
 * all other online players.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class VanishManager {

  private final Plugin plugin;
  private final BossBar bossBar;
  private final Set<Player> vanished;

  /**
   * Instantiates a new manager.
   */
  public VanishManager() {
    this.vanished = new HashSet<>();
    this.plugin = JavaPlugin.getProvidingPlugin(getClass());
    this.bossBar = Bukkit.createBossBar("You're now vanished", BarColor.BLUE, BarStyle.SOLID);
  }

  /**
   * Vanishes a {@link Player} by making them invisible for all other {@link Player}s.
   *
   * @param player the player which needs to be invisible
   */
  public void vanish(Player player) {
    this.bossBar.addPlayer(player);
    player.setCollidable(false);

    if (this.vanished.add(player)) {
      Bukkit.getOnlinePlayers().forEach(target -> target.hidePlayer(this.plugin, player));
    }
  }

  /**
   * Unvanishes a {@link Player} by making them visible to all other {@link Player}s.
   * Should be called whenever someone logs out to prevent memory leaks.
   *
   * @param player the player which needs to be visible
   */
  public void unvanish(Player player) {
    this.bossBar.removePlayer(player);
    player.setCollidable(true);

    if (this.vanished.remove(player)) {
      Bukkit.getOnlinePlayers().forEach(target -> target.showPlayer(this.plugin, player));
    }
  }

  /**
   * Returns {@code true} if the {@link Player} has been vanished, {@code false} otherwise.
   *
   * @param player the player whose status is requested
   * @return {@code true} if the {@link Player} has been vanished, {@code false} otherwise
   */
  public boolean isVanished(Player player) {
    return this.vanished.contains(player);
  }

  /**
   * Returns all {@link Player}s which have been vanished and are online
   */
  public Set<Player> getVanished() {
    return Collections.unmodifiableSet(this.vanished);
  }
}
