package com.gmail.chickenpowerrr.vanish.listener;

import com.gmail.chickenpowerrr.vanish.manager.VanishManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * This listener handles joins to make sure hidden {@link Player}s are
 * hidden for the new {@link Player}s as well.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerJoinEventListener implements Listener {

  private final Plugin plugin;
  private final VanishManager vanishManager;

  /**
   * Instantiates the new listener.
   *
   * @param vanishManager the {@link VanishManager} which handles the {@link Player}s
   *                      who are vanished
   */
  public PlayerJoinEventListener(VanishManager vanishManager) {
    this.vanishManager = vanishManager;
    this.plugin = JavaPlugin.getProvidingPlugin(getClass());
  }

  /**
   * Handles a join by hiding all vanished {@link Player}s from the {@link Player}
   * who just joined the server.
   *
   * @param event the {@link PlayerJoinEvent} which communicates the join of a {@link Player}
   */
  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    this.vanishManager.getVanished().forEach(vanished ->
      event.getPlayer().hidePlayer(this.plugin, vanished));
  }
}
