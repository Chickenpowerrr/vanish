package com.gmail.chickenpowerrr.vanish.listener;

import com.gmail.chickenpowerrr.vanish.manager.VanishManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * This listener handles quits to prevent memory leaks.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerQuitEventListener implements Listener {

  private final VanishManager vanishManager;

  /**
   * Instantiates the new listener.
   *
   * @param vanishManager the {@link VanishManager} which wants their offline
   *                      {@link Player}s to be removed
   */
  public PlayerQuitEventListener(VanishManager vanishManager) {
    this.vanishManager = vanishManager;
  }

  /**
   * Deregisters an offline {@link Player} from the {@link VanishManager}.
   *
   * @param event the {@link PlayerQuitEvent} which communicates the quit of a {@link Player}
   */
  @EventHandler
  public void onQuit(PlayerQuitEvent event) {
    this.vanishManager.unvanish(event.getPlayer());
  }
}
