package com.gmail.chickenpowerrr.vanish.command;

import com.gmail.chickenpowerrr.vanish.manager.VanishManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This {@link CommandExecutor} handles the vanish command by allowing
 * {@link Player}s to enable, disable or toggle their vanish status.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class VanishCommandExecutor implements CommandExecutor {

  private final VanishManager vanishManager;

  /**
   * Instantiates the new {@link CommandExecutor}.
   *
   * @param vanishManager the {@link CommandExecutor} which handles the
   *                      vanish status for the {@link Player}s
   */
  public VanishCommandExecutor(VanishManager vanishManager) {
    this.vanishManager = vanishManager;
  }

  /**
   * Allows three sub-commands: enable, disable and toggle. If no subcommand is provided,
   * it will default to toggle. This command is only available for {@link Player}s.
   *
   * @param commandSender the {@link CommandSender} which invoked the command
   * @param command the {@link Command} as seen by Spigot
   * @param s the label which was used to trigger the command
   * @param args the arguments passed when invoking the command
   * @return {@code true}
   */
  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
    if (!(commandSender instanceof Player)) {
      commandSender.sendMessage(ChatColor.RED + "Only players are able to use this command");
      return true;
    }

    Player player = (Player) commandSender;
    if (args.length == 0) {
      toggle(player);
      return true;
    }

    switch (args[0].toLowerCase()) {
      case "enable":
        enable(player);
        break;
      case "disable":
        disable(player);
        break;
      case "toggle":
        // Fall through
      default:
        toggle(player);
        break;
    }
    return true;
  }

  /**
   * Enables vanish for a {@link Player}.
   *
   * @param player the {@link Player} who wants to enable their vanish status
   */
  private void enable(Player player) {
    this.vanishManager.vanish(player);
  }

  /**
   * Disables vanish for a {@link Player}.
   *
   * @param player the {@link Player} who wants to disable their vanish status
   */
  private void disable(Player player) {
    this.vanishManager.unvanish(player);
  }

  /**
   * Toggles vanish for a {@link Player}.
   *
   * @param player the {@link Player} who wants to toggle their vanish status
   */
  private void toggle(Player player) {
    if (this.vanishManager.isVanished(player)) {
      disable(player);
    } else {
      enable(player);
    }
  }
}
