package com.gmail.chickenpowerrr.vanish;

import com.gmail.chickenpowerrr.vanish.command.VanishCommandExecutor;
import com.gmail.chickenpowerrr.vanish.listener.PlayerJoinEventListener;
import com.gmail.chickenpowerrr.vanish.listener.PlayerQuitEventListener;
import com.gmail.chickenpowerrr.vanish.manager.VanishManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class for the plugin.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class VanishPlugin extends JavaPlugin {

  /**
   * Registers the events and the commands.
   */
  @Override
  public void onEnable() {
    VanishManager vanishManager = new VanishManager();

    Bukkit.getPluginManager().registerEvents(new PlayerJoinEventListener(vanishManager), this);
    Bukkit.getPluginManager().registerEvents(new PlayerQuitEventListener(vanishManager), this);

    Bukkit.getPluginCommand("vanish").setExecutor(new VanishCommandExecutor(vanishManager));
  }

  @Override
  public void onDisable() {

  }
}
